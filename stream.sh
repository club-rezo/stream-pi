#!/bin/bash
echo "hostname=`hostname`" | curl -k -d @- https://secure.club-rezo.net/rasp/hostname
amixer cset numid=3 1

while :
do

NOW=$(date +"%T")
echo ""
echo "$NOW - Launching stream on $HOSTNAME"
echo ""

omxplayer -o hdmi -r -b --no-osd --live rtmp://rtmp.club-rezo.net:1935/liverasp/stream
sleep 1

NOW=$(date +"%T")
echo ""
echo "$NOW - Stream failed on $HOSTNAME"
echo ""

done